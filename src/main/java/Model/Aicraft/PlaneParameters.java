package Model.Aicraft;

/**
 * This class handles all parameters for planes constructors.
 * Object of this class carries all data that is required
 * to create a new an instance of Plane.
 */

public class PlaneParameters {
  private int numberOfSeats;
  private double massOfCargo;
  private double fuelTank;
  private String planeName;

  public void setNumberOfSeats(int numberOfSeats) {
    this.numberOfSeats = numberOfSeats;
  }

  public int getNumberOfSeats() {
    return numberOfSeats;
  }

  public void setMassOfCargo(double massOfCargo) {
    this.massOfCargo = massOfCargo;
  }

  public double getMassOfCargo() {
    return massOfCargo;
  }

  public void setFuelTank(double fuelTank) {
    this.fuelTank = fuelTank;
  }

  public double getFuelTank() {
    return fuelTank;
  }

  public void setPlaneName(String planeName) {
    this.planeName = planeName;
  }

  public String getPlaneName() {
    return planeName;
  }
}
