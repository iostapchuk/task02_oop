package Model.Aicraft;

import Model.JetEngine;

/**
 * There are no setters as changing a parameter of
 * one aircraft essentially makes it a different aircraft.
 */

public class HeavyJet extends Plane implements JetEngine{
  /**
   * Number of engines defines most things about the plane
   * that's why it is a constant - change number of engines
   * and you will get other type of plane.
   */
  public final static double numberOfEngines = 4;
  private int passengersSeats;
  private double cargoMass; // in kilograms
  private double fuelTankSize; // in kilograms
  private double fuelConsumtion; //in kilograms of fuel per kilometer of distance, calculated
  private double flightCost; // in dollars, calculated
  private double flightRange; // in kilometres, calculated
  private String planeName;

  public HeavyJet(PlaneParameters params) {
    this.passengersSeats = params.getNumberOfSeats();
    this.cargoMass = params.getNumberOfSeats();
    this.fuelTankSize = params.getFuelTank();
    this.planeName = params.getPlaneName();
    calculateFuelConsumption();
    calculateFlightCost();
  }

  private void calculateFuelConsumption() {
    double modifier = 10_000; // to make cargo mass number appropriate for the formula
    /* assume that mass of cargo that is less than 50 000 kg
     has no effect on fuel consumption
    */
    double noEffectMass = 50_000;
    this.fuelConsumtion = ((this.cargoMass - noEffectMass) / modifier) * (JET_FUEL_CONSUMPTION * numberOfEngines);
  }

  private void calculateFlightCost() {
    this.flightCost = numberOfEngines * JET_COST * (this.cargoMass / 1.6);
  }

  private void calculateRange() {
    // formula calculates range is thousand of kilometers so we have to multiply it by 1000
    double perThousand = 1000;
    this.flightRange = (this.fuelTankSize / this.fuelConsumtion) * perThousand;
  }

  public void setPlaneName(String planeName) {
    this.planeName = planeName;
  }

  public double getCargoMass() {
    return this.cargoMass;
  }

  public double getFlightCost() {
    return this.flightCost;
  }

  public double getFlightRange() {
    return this.flightRange;
  }

  public double getFuelConsumtion() {
    return this.fuelConsumtion;
  }

  public double getFuelTankSize() {
    return this.fuelTankSize;
  }

  public int getPassengersSeats() {
    return this.passengersSeats;
  }

  public String getPlaneName() {
    return this.planeName;
  }

  @Override
  public String toString() {
    String planeString = "Name of the plane: " + this.getPlaneName()
        + "\tRange: " + this.getFlightRange()
        + "\tNumber of passenger seats: " + this.getPassengersSeats()
        + "\tPayload mass: " + this.getCargoMass()
        +  "\tFuel consumption: " + this.getFuelConsumtion()
        + "\tCost of one flight: " + this.getFlightCost() + ".";
    return planeString;
  }
}
