package Model.Aicraft;

/**
 * This class is here to handle 3 types of planes
 * in one array while not handling jet engines.
 * Using interface JetEngine would be just a bit confusing.
 */

public abstract class Plane {
  protected int passengersSeats;
  protected double cargoMass; // in kilograms
  protected double fuelTankSize; // in kilograms
  protected double fuelConsumtion; //in kilograms of fuel per kilometer of distance, calculated
  protected double flightCost; // in dollars, calculated
  protected double flightRange; // in kilometres, calculated
  protected String planeName;

  public double getCargoMass() {
    return this.cargoMass;
  }

  public double getFlightCost() {
    return this.flightCost;
  }

  public double getFlightRange() {
    return this.flightRange;
  }

  public double getFuelConsumtion() {
    return this.fuelConsumtion;
  }

  public double getFuelTankSize() {
    return this.fuelTankSize;
  }

  public int getPassengersSeats() {
    return this.passengersSeats;
  }

  public String getPlaneName() {
    return this.planeName;
  }

  @Override
  public String toString() {
    String planeString = "Name of the plane: " + this.getPlaneName()
        + "\tRange: " + this.getFlightRange()
        + "\tNumber of passenger seats: " + this.getPassengersSeats()
        + "\tPayload mass: " + this.getCargoMass()
        +  "\tFuel consumption: " + this.getFuelConsumtion()
        + "\tCost of one flight: " + this.getFlightCost() + ".";
    return planeString;
  }
}
