package Model;

import Controller.Controller;
import Model.Aicraft.*;
import View.ViewReceiver;

/**
 * Class, where all business logic happens.
 */

public class Airline {
  private Plane[] planes = new Plane[1];
  private ViewReceiver airlineView = new ViewReceiver();
  private Controller airlineController = new Controller();

  public void start(){
    airlineView.showMessage("For airline to exist it has to have at least one plane." +
        "\nPlease, create this plane. Please choose between the types of planes.");
    airlineView.showPlanesDescription();
    createFirstPlane();
    airlineView.showMessage("You have successfully created you an airline and a plane."
        + "Now going to the menu...");
    airlineView.planeCreated();
    airlineController.mainMenu();
  }

  // Returning sum of all passenger seats of all planes
  public int getAllPassengerCapacity(){
    int passengerSeatsSum = 0;
    for (int i = 0; i < planes.length; i++) {
      passengerSeatsSum += planes[i].getPassengersSeats();
    }
    return passengerSeatsSum;
  }

  // Returning sum of all cargo payloads of all planes
  public double getAllCargoCapacity(){
    double cargoCapacitySum = 0.0;
    for (Plane aircraft: planes) {
      cargoCapacitySum += aircraft.getPassengersSeats();
    }
    return cargoCapacitySum;
  }

  public String[] getSortedPlanesList(){
    Plane[] sortedPlanes = planes;
    rangeSortingPlanes(sortedPlanes);
    String[] listOfPlanes = new String[sortedPlanes.length];
    for (int i = 0; i < sortedPlanes.length; i++) {
      listOfPlanes[i] = sortedPlanes[i].toString();
    }
    return listOfPlanes;
  }

  public void addNewPlane() {
    airlineView.newPlane();
    airlineView.showPlanesDescription();
     /*
    For is in this form so user can make mistakes until they input a number.
    That's the only technique I know, but I am open to new ideas.
     */
    for( ; ; ) {
      int planeType = airlineController.inputIntAndReturn();
      if(planeType == 1) {
        createSmallPlane();
        airlineView.planeCreated();
        return;
      } else if(planeType == 2) {
        createMediumPlane();
        airlineView.planeCreated();
        return;
      } else if(planeType == 3) {
        createHeavyPlane();
        airlineView.planeCreated();
        return;
      } else {
        airlineView.numberNotOnList();
      }
    }
  }

  private void rangeSortingPlanes(Plane[] sortPlanes){
    //bubble sort
    boolean isSorted = true;
    Plane buffer;
    while(!isSorted) {
      for (int i = 0; i < sortPlanes.length - 1; i++) {
        if (sortPlanes[i].getFlightRange() > sortPlanes[i + 1].getFlightRange()) {
          Plane temp = sortPlanes[i];
          sortPlanes[i] = sortPlanes[i + 1];
          sortPlanes[i + 1] = temp;
          isSorted = false;
        }
      }
    }
  }

  private void createFirstPlane() {
    /*
    For is in this form so user can make mistakes until they input a number.
    That's the only technique I know, but I am open to new ideas.
     */
    //TODO move these messages to Subscribers.
    for( ; ; ) {
      int typeOfPlane = airlineController.inputIntAndReturn();
      if (typeOfPlane == 1) {
        airlineView.showMessage("You decided to add a small plane. "
            + "Now please type in plane's parameters.");
        planes[0] = new SmallJet(makePlaneParameters());
        return;
      } else if (typeOfPlane == 2) {
        airlineView.showMessage("You decided to add a medium plane. "
            + "Now please type in plane's parameters.");
        planes[0] = new MediumJet(makePlaneParameters());
        return;
      } else if (typeOfPlane == 3) {
        airlineView.showMessage("You decided to add a heavy plane. "
            + "Now please type in plane's parameters.");
        planes[0] = new HeavyJet(makePlaneParameters());
        return;
      } else {
        airlineView.numberNotOnList();
      }
    }
  }

  private void createHeavyPlane(){
    extendPlanesArray();
    planes[planes.length-1] = new HeavyJet(makePlaneParameters());
  }

  private void createMediumPlane(){
    extendPlanesArray();
    planes[planes.length-1] = new MediumJet(makePlaneParameters());
  }

  private void createSmallPlane(){
    extendPlanesArray();
    planes[planes.length-1] = new SmallJet(makePlaneParameters());
  }

  private void extendPlanesArray(){
    Plane[] tempPlanes = new Plane[planes.length + 1];
    for(int i = 0; i < planes.length; i++){
      tempPlanes[i] = planes[i];
    }
    planes = tempPlanes;
  }

  private PlaneParameters makePlaneParameters(){
    PlaneParameters parameters = new PlaneParameters();
    airlineView.askPlaneName();
    parameters.setPlaneName(airlineController.inputStringAndReturn());
    airlineView.askNumberOfSeats();
    parameters.setNumberOfSeats(airlineController.inputIntAndReturn());
    airlineView.askMassOfCargo();
    parameters.setMassOfCargo(airlineController.inputDoubleAndReturn());
    airlineView.askTankCapacity();
    parameters.setFuelTank(airlineController.inputDoubleAndReturn());
    return parameters;
  }

  public int getNumberOfPlanes(){
    return planes.length;
  }
}
