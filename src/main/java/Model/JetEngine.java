package Model;

/**
 * This interface defines the engine of the aircraft.
 * All data is and assumption.
 * Every aircraft will have some number of jet engines
 * and it will define types of planes - hence aircraft hierarchy (somewhat).
 */

public interface JetEngine {
  double JET_FUEL_CONSUMPTION = 10; // in kilograms per 1 kilometer
  double JET_COST = 15_000; // in dollars
}
