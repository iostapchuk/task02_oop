package View;

/**
 * This inteface makes working with multiple View modules possible.
 * All methods here are just messages that are essential for proper using of View module.
 */

public interface Subscriber {
  void showMessage(String message);
  void showPlanesDescription();
  void generateInitialMessage();
  void askNumberOfSeats();
  void askMassOfCargo();
  void askTankCapacity();
  void askPlaneName();
  void showMainMenu();
  void didSomethingWrong();
  void numberNotOnList();
  void inputNotANumber();
  void showPassengerCapacity(int passengerSeats);
  void showCargoCapacity(double cargoCapacity);
  void showSortedByRange();
  void typeInRespectiveNumber();
  void planeCreated();
  void goodbye();
  void newPlane();
}
