package View;

/**
 * This class here is just to send commands to all View modules available.
 * It implements interface Subscriber just because it it easier to keep
 * track of all changes is Subscriber interface.
 * Implementation will be removed with the release.
 */

public class ViewReceiver implements Subscriber{
  static private Subscriber subsribers[] = new Subscriber[1];

  public ViewReceiver(){
    ViewConsole view = new ViewConsole();
    subsribers[0] = view;
  }

  public void addSubscriber(ViewConsole view){
    Subscriber subscribersTemp[] = new Subscriber[subsribers.length + 1];
    for(int i = 0; i < subsribers.length; i++) {
      subscribersTemp[i] = subsribers [i];
    }
    subscribersTemp[subscribersTemp.length - 1] = view;
    subsribers = subscribersTemp;
  }

  public void showMessage(String message){
    for(Subscriber subs : subsribers){
      subs.showMessage(message);
    }
  }

  public void showPlanesDescription(){
    for(Subscriber subs : subsribers){
      subs.showPlanesDescription();
    }
  }

  public void generateInitialMessage(){
    for(Subscriber subs : subsribers){
      subs.generateInitialMessage();
    }
  }

  public void askNumberOfSeats(){
    for(Subscriber subs : subsribers){
      subs.askNumberOfSeats();
    }
  }
  public void askMassOfCargo(){
    for(Subscriber subs : subsribers){
      subs.askMassOfCargo();
    }
  }
  public void askTankCapacity(){
    for(Subscriber subs : subsribers){
      subs.askTankCapacity();
    }
  }

  public void askPlaneName(){
    for(Subscriber subs : subsribers){
      subs.askPlaneName();
    }
  }

  public void showMainMenu(){
    for(Subscriber subs : subsribers){
      subs.showMainMenu();
    }
  }

  public void didSomethingWrong(){
    for(Subscriber subs : subsribers){
      subs.didSomethingWrong();
    }
  }

  public void numberNotOnList(){
    for(Subscriber subs : subsribers){
      subs.numberNotOnList();
    }
  }

  public void inputNotANumber(){
    for(Subscriber subs : subsribers){
      subs.inputNotANumber();
    }
  }

  public void showPassengerCapacity(int passengerSeats){
    for(Subscriber subs : subsribers){
      subs.showPassengerCapacity(passengerSeats);
    }
  }

  public void showCargoCapacity(double cargoCapacity){
    for(Subscriber subs : subsribers){
      subs.showCargoCapacity(cargoCapacity);
    }
  }

  public void showSortedByRange() {
    for(Subscriber subs : subsribers){
      subs.showSortedByRange();
    }
  }

  public void typeInRespectiveNumber() {
    for(Subscriber subs : subsribers){
      subs.typeInRespectiveNumber();
    }
  }

  public void planeCreated() {
    for(Subscriber subs : subsribers){
      subs.planeCreated();
    }
  }

  public void goodbye() {
    for(Subscriber subs : subsribers){
      subs.goodbye();
    }
  }

  public void newPlane() {
    for(Subscriber subs : subsribers){
      subs.newPlane();
    }
  }
}
