package View;

/**
 * Just a representation of one of many possible View modules.
 * This one works in console.
 */

class ViewConsole implements Subscriber {

  public void showMessage(String message){
    System.out.println(message);
  }

  public void showPlanesDescription(){
    System.out.println("There are three types of planes:");
    System.out.println("1. Small plane - one jet engine, small range, small capacity, cheap.");
    System.out.println("2. Medium plane - two engines, medium in all categories.");
    System.out.println("3. Heavy plane - four jet engines, biggest range and capacity, very expensive.");
    System.out.println("Type in one of these numbers to make a decision:");
  }

  public void generateInitialMessage(){
    System.out.println("Hello, this is a program about planes and airlines." +
        "\nFirst of all, you have to create an airline.");
  }

  public void askNumberOfSeats(){
    System.out.println("Please type in the number of passenger seats of your plane: ");
  }
  public void askMassOfCargo() {
    System.out.println("Please type in the payload mass of your plane: ");
  }
  public void askTankCapacity() {
    System.out.println("Please type in tank capacity of your plane: ");
  }

  public void askPlaneName() {
    System.out.println("Please type in the name of your plane: ");
  }

  public void showMainMenu() {
    System.out.println("Main menu.");
    System.out.println("1. Add new plane to the airline.");
    System.out.println("2. Print passenger capacity of all planes of the airline.");
    System.out.println("3. Print cargo mass capacity of  all planes of the airline.");
    System.out.println("4. Print list of planes sorted by range.");
    System.out.println("5. Search planes by a given range of fuel consumption.");
    System.out.println("0. Exit");
  }

  public void didSomethingWrong() {
    System.out.println("Oops, you did something wrong! Try again.");
  }

  public void numberNotOnList() {
    System.out.println("You typed in a number that is not on the list! Try again.");
  }

  public void inputNotANumber() {
    System.out.println("You typed in something that is not a number! Try again.");
  }

  public void showPassengerCapacity(int passengerSeats) {
    System.out.println("Number of all seats in all planes is: " + passengerSeats);
  }

  public void showCargoCapacity(double cargoCapacity) {
    System.out.println("Number of cargo payload capacity of all planes is: " + cargoCapacity);
  }

  public void showSortedByRange() {
    // just the first sentence, the table gets from Controller through message method
    System.out.println("Printing list of planes sorted by their range...");
  }

  public void typeInRespectiveNumber() {
    System.out.println("Please type in a respective number down below:");
  }

  public void planeCreated() {
    System.out.println("Plane created!");
  }

  public void goodbye() {
    System.out.println("Exiting... Goodbye!");
  }

  public void newPlane() {
    System.out.println("You decided to add new plane.");
  }
}
