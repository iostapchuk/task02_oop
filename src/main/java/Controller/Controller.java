package Controller;

import Model.Airline;
import View.ViewReceiver;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * This class handles all data input from user and it sends commands to
 * Model and View modules.
 */

public class Controller {
  private ViewReceiver view = new ViewReceiver();
  private static Airline airline;

  public void start() {
    view.generateInitialMessage();
    airline = new Airline();
    airline.start();
  }

  public void mainMenu() {
    for( ; ; ) {
      view.showMainMenu();
      int choice = inputIntAndReturn();
      if(choice == 1) {
        airline.addNewPlane();
      } else if(choice == 2) {
        view.showPassengerCapacity(airline.getAllPassengerCapacity());
      } else if(choice == 3) {
        view.showCargoCapacity(airline.getAllCargoCapacity());
      } else if(choice == 4) {
        view.showSortedByRange();
        String[] sortedList = airline.getSortedPlanesList();
        for(String plane : sortedList) {
          view.showMessage(plane);
        }
      } else if(choice == 5) {
        //TODO
      } else if(choice == 0) {
        view.goodbye();
        return;
      } else {
        view.didSomethingWrong();
      }
    }
  }

  public void addPlane(){
    airline.addNewPlane();
  }

  public void showPassCapacity(){
    view.showPassengerCapacity(airline.getAllPassengerCapacity());
  }

  public void showCargoCapacity(){
    view.showCargoCapacity(airline.getAllCargoCapacity());
  }

  public void showSortedPlanesList(){
    view.showSortedByRange();
    String[] sortedList = airline.getSortedPlanesList();
    for(int i = 0; i < sortedList.length; i++) {
      view.showMessage(sortedList[i]);
    }
  }

  public void searchByFuelConsumption(){
    //TODO implement
  }

  public int inputIntAndReturn(){
    BufferedReader reader =
        new BufferedReader(new InputStreamReader(System.in));
    /*
    For is in this form so user can make mistakes until they input a number.
    That's the only technique I know, but I am open to new ideas.
     */
    for( ; ; ) {
      try {
        String inputString = reader.readLine();
        return Integer.parseInt(inputString);
      } catch(IOException io) {
        view.didSomethingWrong();
      } catch(NumberFormatException numb) {
        view.inputNotANumber();
      }
    }
  }

  public double inputDoubleAndReturn(){
    BufferedReader reader =
        new BufferedReader(new InputStreamReader(System.in));
    /*
    For is in this form so user can make mistakes until they input a number.
    That's the only technique I know, but I am open to new ideas.
     */
    for( ; ; ) {
      try {
        String inputString = reader.readLine();
        return Double.parseDouble(inputString);
      } catch(IOException io) {
        view.didSomethingWrong();
      } catch(NumberFormatException numb) {
        view.inputNotANumber();
      }
    }
  }

  public String inputStringAndReturn(){
    BufferedReader reader =
        new BufferedReader(new InputStreamReader(System.in));
    /*
    For is in this form so user can make mistakes until they input a number.
    That's the only technique I know, but I am open to new ideas.
     */
    for( ; ; ) {
      try {
        String inputString = reader.readLine();
        return inputString;
      } catch(IOException io) {
        view.didSomethingWrong();
      }
    }
  }

  // For just some unspecified message
  public void messageToView(String message){
    view.showMessage(message);
  }
}
