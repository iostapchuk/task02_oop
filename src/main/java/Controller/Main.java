package Controller;

/**
 * Pre-release.
 *
 * @version 0.8
 * @author Ivan Ostapchuk ivan.ostapchuk.27@gmail.com
 * @since 05.04.2019
 */

public class Main {
  public static void main(String[] args) {
    Controller controller = new Controller();
    controller.start();
  }
}
